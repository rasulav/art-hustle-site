import mainVideoImage from "../../../assets/images/lesson/mainVideo.svg";
import classes from "./Viewer.module.scss";

interface ViewerProps {
  videoLink?: string;
}

export const Viewer: React.FC<ViewerProps> = (props) => {
  return (
    <a href={props.videoLink} target="_blank" rel="noreferrer">
      <div
        style={{
          backgroundImage: `url(${mainVideoImage})`,
        }}
        className={classes.videoImage}
      ></div>
    </a>
  );
};
