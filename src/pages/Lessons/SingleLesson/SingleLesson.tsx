import { Container } from "react-bootstrap";
import axios from "../../../service/axios";

import VideoViewer from "../../../components/VideoViewer";
import AboutLesson from "../../../components/AboutLesson";
import SuspenseSpinner from "../../../components/SuspenseFallback";
import Suggestions from "../../../components/Suggestions";

import classes from "./SingleLesson.module.scss";
import { useEffect, useState } from "react";

export const SingleLesson: React.FC = () => {
  const [lessonParams, setLessonParams] = useState({
    title: "",
    about: { title: "", description: "" },
    author: { name: "", specialization: "", quote: "" },
    videoParams: {
      currentVideoLink: "",
      currentVideoId: null,
      totalDuration: null,
      tags: [],
    },
    videos: [],
    suggestions: [],
  });
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  const fetchLessonData = async () => {
    return await axios.get("/lessonParams");
  };

  useEffect(() => {
    fetchLessonData()
      .then((response) => {
        setLessonParams(response.data);
        setLoading(false);
      })
      .catch((err) => {
        setError(err.message);
        setLoading(false);
      });
  }, []);

  return !loading ? (
    <Container className="p-5">
      {!error ? (
        <h1 className={classes.title}>{lessonParams.title}</h1>
      ) : (
        <p className={classes.error}>Something went wrong: {error}</p>
      )}
      <VideoViewer
        videos={lessonParams.videos}
        videoParams={lessonParams.videoParams}
      />
      <AboutLesson about={lessonParams.about} author={lessonParams.author} />
      <Suggestions suggestions={lessonParams.suggestions} />
    </Container>
  ) : (
    <SuspenseSpinner />
  );
};
