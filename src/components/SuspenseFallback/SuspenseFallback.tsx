import React from "react";
import { Spinner } from "react-bootstrap";
import classes from "./SuspenseFallback.module.scss";

export const SuspenseFallback: React.FC = () => {
  return (
    <div className={classes.supenseContainer}>
      <Spinner animation="border" />
    </div>
  );
};
