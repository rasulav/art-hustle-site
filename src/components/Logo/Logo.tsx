import { Navbar } from "react-bootstrap";
import classes from "./Logo.module.scss";

export const Logo: React.FC<{ customClass?: string }> = (props) => {
  let customClassName = classes.logo;
  if (props.customClass) {
    customClassName += " " + props.customClass;
  }

  return (
    <Navbar.Brand href="#home" className={customClassName}>
      <span>art</span>hustle
    </Navbar.Brand>
  );
};
