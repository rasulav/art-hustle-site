import { Col, ListGroup, Row } from "react-bootstrap";
import Viewer from "./Viewer";
import VideoSelector from "./VideoSelector";

import classes from "./VideoViewer.module.scss";

interface video {
  id: number;
  name: string;
  duration: string;
  link: string;
}
interface videosParams {
  currentVideoLink: string;
  currentVideoId: number | null;
  totalDuration: number | null;
  tags: string[];
}
interface VideoViewerProps {
  videos: video[];
  videoParams: videosParams;
}

export const VideoViewer: React.FC<VideoViewerProps> = (props) => {
  return (
    <Row>
      <Col md={12} lg={8}>
        <ListGroup className={classes.tagsList} horizontal>
          {props.videoParams.tags.map((tag, i) => (
            <ListGroup.Item key={i} className={classes.tag}>
              {tag}
            </ListGroup.Item>
          ))}
        </ListGroup>

        <Viewer videoLink={props.videoParams.currentVideoLink} />
      </Col>
      <Col>
        <ListGroup className={classes.videoSelectorInfo} horizontal>
          <ListGroup.Item className={classes.count}>
            {props.videos.length} lessons (
            {props.videoParams.totalDuration} m)
          </ListGroup.Item>
          <ListGroup.Item className={classes.lang}>
            Audio language:
          </ListGroup.Item>
        </ListGroup>

        <VideoSelector
          videos={props.videos}
          current={props.videoParams.currentVideoId}
        />
      </Col>
    </Row>
  );
};
