import React from "react";

export const Layout: React.FC<{ header: JSX.Element; footer?: JSX.Element }> = (
  props
) => {
  return (
    <>
      <header>{props.header}</header>
      <main>{props.children}</main>
      <footer>{props.footer}</footer>
    </>
  );
};
