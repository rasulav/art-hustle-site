import { render, act } from "@testing-library/react";
import axios from "../../../service/axios";

import SingleLesson from "../SingleLesson";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("Single lesson page", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("loads lesson params", async () => {
    render(<SingleLesson />);
    
    await act(async () => {
      await mockedAxios.get.mockImplementationOnce(() =>
        Promise.resolve({ data: {} })
      );
    });

    await expect(mockedAxios.get).toHaveBeenCalledWith("/lessonParams");
    await expect(mockedAxios.get).toHaveBeenCalledTimes(1);
  });
});
