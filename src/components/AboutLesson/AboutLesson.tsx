import { useState } from "react";
import { Tabs, Tab, Row, Stack, Button, Col } from "react-bootstrap";
import autorImage from "../../assets/images/lesson/author_mock.jpg";
import addImage from "../../assets/images/lesson/add_material_mock.jpg";

import classes from "./AboutLesson.module.scss";

interface author {
  name: string;
  specialization: string;
  quote: string;
}
interface about {
  title: string;
  description: string;
}
interface AboutLessonProps {
  author: author;
  about: about;
}

export const AboutLesson: React.FC<AboutLessonProps> = (props) => {
  const [activeTab, setActiveTab] = useState<string | null>("about");

  return (
    <div className="mt-5">
      <Tabs
        defaultActiveKey="about"
        className={classes.tabs}
        onSelect={(k) => setActiveTab(k)}
      >
        <Tab
          eventKey="about"
          title="About Course"
          tabClassName={`${classes.tab} ${
            activeTab === "about" && classes.activeTab
          }`}
        >
          <Row>
            <Col md={12} lg={8}>
              <Stack gap={3} className={classes.aboutStack}>
                <div className={classes.title}>{props.about.title}</div>
                <div className={classes.description}>
                  {props.about.description}
                </div>
                <div>
                  <Button className={classes.button}>
                    To course description
                  </Button>
                </div>
              </Stack>
            </Col>
            <Col md={12} lg={4}>
              <Stack
                direction="horizontal"
                gap={3}
                className={classes.authorStack}
              >
                <div>
                  <div
                    className={classes.image}
                    style={{
                      backgroundImage: `url(${autorImage})`,
                    }}
                  ></div>
                </div>
                <div>
                  <div className={classes.name}>{props.author.name}</div>
                  <div className={classes.specialization}>
                    {props.author.name}
                  </div>
                  <div className={classes.quote}>{props.author.quote}</div>
                </div>
              </Stack>
            </Col>
          </Row>
        </Tab>
        <Tab
          eventKey="additional"
          title="Additional Materials"
          tabClassName={`${classes.tab} ${
            activeTab === "additional" && classes.activeTab
          }`}
        >
          <div className={classes.additional}>
            <div
              className={classes.image}
              style={{
                backgroundImage: `url(${addImage})`,
              }}
            ></div>
          </div>
        </Tab>
      </Tabs>
    </div>
  );
};
