import { Button, Col, Container, ListGroup, Row } from "react-bootstrap";
import classes from "./Footer.module.scss";
import Logo from "../Logo";

export const Footer = () => {
  return (
    <div className={classes.footer}>
      <Container className="pb-5">
        <Row>
          <Col md={12} lg={8} className={classes.menu}>
            <Row>
              <Col className={classes.left}>
                <Logo customClass={classes.logo} />
                <p className={classes.slogan}>
                  Far far away, behind the word mountains, far from the
                  countries Vokalia and Consonantia
                </p>
              </Col>
              <Col className={classes.center}>
                <ListGroup variant="flush">
                  <ListGroup.Item>Courses</ListGroup.Item>
                  <ListGroup.Item>Blog</ListGroup.Item>
                </ListGroup>
              </Col>
              <Col className={classes.right}>
                <ListGroup>
                  <ListGroup.Item>Become a teacher</ListGroup.Item>
                  <ListGroup.Item>Contact us</ListGroup.Item>
                  <ListGroup.Item>Careers</ListGroup.Item>
                </ListGroup>
              </Col>
            </Row>
          </Col>
          <Col md={12} lg={4} className={classes.actions}>
            <ListGroup>
              <ListGroup.Item>
                <Button>write us</Button>
              </ListGroup.Item>
              <ListGroup.Item>
                <Button>subscribe</Button>
              </ListGroup.Item>
            </ListGroup>
          </Col>
        </Row>
      </Container>
      <div className={classes.line} />
      <Container className="pt-2">
        <Row className={classes.bottom}>
          <Col>
            <ListGroup horizontal>
              <ListGroup.Item>Privacy Policy</ListGroup.Item>
              <ListGroup.Item>Terms or Use</ListGroup.Item>
              <ListGroup.Item>Impressum</ListGroup.Item>
            </ListGroup>
          </Col>
          <Col>
            <Row>
              <Col md={12} lg={8}></Col>
              <Col md={12} lg={4}>
                <ListGroup.Item>© 2020 ArtHustle</ListGroup.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
