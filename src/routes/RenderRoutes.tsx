import { lazy } from "react";
import { Route, Routes } from "react-router-dom";
const Lessons = lazy(() => import("../pages/Lessons"));
const SingleLesson = lazy(() => import("../pages/Lessons/SingleLesson"));

type routeItem = {
  path: string;
  key: string;
  component: Function;
};

type routes = routeItem & {
  routes?: routeItem[];
};

const ROUTES: routes[] = [
  {
    path: "/",
    key: "LESSON",
    component: SingleLesson,
  },
  {
    path: "/lessons",
    key: "LESSONS",
    component: Lessons,
  },
];

export default ROUTES;

export function RenderRoutes({ routes }: { routes: routes[] }) {
  return (
    <Routes>
      {routes.map((route, i) => {
        return (
          <Route
            key={route.key}
            path={route.path}
            element={<route.component />}
          />
        );
      })}

      <Route element={<h1>Not Found!</h1>} />
    </Routes>
  );
}
