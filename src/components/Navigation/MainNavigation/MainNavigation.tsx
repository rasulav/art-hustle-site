import { useState } from "react";
import {
  Container,
  Nav,
  Navbar,
  NavDropdown,
  Image,
  Stack,
  Button,
} from "react-bootstrap";
import classes from "./MainNavigation.module.scss";

import Logo from "../../Logo";
import globeIcon from "../../../assets/images/globe.svg";
import searchIcon from "../../../assets/images/search.svg";

export const MainNavigation: React.FC = () => {
  const [buttonText, setButtonText] = useState("Login");

  const handleButtonTextChange = () => {
    setButtonText("Logout");
  };

  return (
    <Navbar collapseOnSelect expand="lg" className={classes.header}>
      <Container>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Logo />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className={`${classes.leftMenu} me-auto`}>
            <Stack direction="horizontal" gap={2}>
              <Nav.Link href="#courses">Courses</Nav.Link>
              <Nav.Link href="#blog">Blog</Nav.Link>
            </Stack>
          </Nav>
        </Navbar.Collapse>
        <Nav className={classes.rightMenu}>
          <Stack direction="horizontal" gap={2}>
            <Image src={searchIcon} />
            <Nav.Link href="#teacher">Become a teacher</Nav.Link>
            <Button
              variant="outline-info"
              name="login"
              onClick={handleButtonTextChange}
              className={classes.loginButton}
            >
              {buttonText}
            </Button>

            <NavDropdown
              title={
                <span>
                  <Image src={globeIcon} className={classes.globeIcon} />
                  EN
                </span>
              }
              id="collasible-nav-dropdown"
              className={classes.dropdown}
            >
              <NavDropdown.Item href="#action/4.1">EN</NavDropdown.Item>
              <NavDropdown.Item href="#action/4.2">DE</NavDropdown.Item>
              <NavDropdown.Item href="#action/4.3">RU</NavDropdown.Item>
            </NavDropdown>
          </Stack>
        </Nav>
      </Container>
    </Navbar>
  );
};
