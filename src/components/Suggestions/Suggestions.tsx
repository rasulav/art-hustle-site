import { Card, Col, Row } from "react-bootstrap";
import classes from "./Suggestions.module.scss";

import cardImage from "../../assets/images/lesson/suggestions_mock_1_268x180.jpg";

interface suggestions {
  title: string;
  author: string;
  oldPrice?: number;
  newPrice: number;
  progress: number;
}
interface SuggestionsProps {
  suggestions: suggestions[];
}

export const Suggestions: React.FC<SuggestionsProps> = (props) => {
  return (
    <div className="mt-5">
      <h2 className={classes.mainTitle}>Similar courses</h2>
      <div className={classes.cards}>
        {props.suggestions.map((card, i) => (
          <Card key={i} className={classes.card}>
            <div className={classes.thumbnail}>
              <Card.Img variant="top" src={cardImage} />
              <div className={classes.wish}></div>
              <div className={classes.progress}>{card.progress}m</div>
            </div>
            <Card.Body>
              <Row>
                <Col xs={10}>
                  <Card.Title className={classes.title}>
                    {card.title}
                  </Card.Title>
                  <Card.Text className={classes.author}>
                    {card.author}
                  </Card.Text>
                </Col>
                <Col xs={2}>
                  <Card.Text className={classes.price}>
                    <span className={classes.new}>{card.newPrice}$</span>
                    <span className={classes.old}>{card.oldPrice}$</span>
                  </Card.Text>
                </Col>
              </Row>
            </Card.Body>
          </Card>
        ))}
      </div>
    </div>
  );
};
