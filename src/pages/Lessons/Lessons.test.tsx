import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import Lessons from "../Lessons";

describe("Lessons component", () => {
  test('renders "Lessons" title as a text', () => {
    render(<Lessons />);
    
    const lessonText = screen.getByText("Lessons", { exact: false });
    expect(lessonText).toBeInTheDocument();
  });
});
