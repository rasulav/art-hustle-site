import { Component, ReactNode, ErrorInfo } from "react";
import classes from "./ErrorBoundaries.module.scss";

interface Props {
  children: ReactNode;
}
interface State {
  hasError: boolean;
  error: string;
}

export class ErrorBoundaries extends Component<Props, State> {
  public state: State = {
    hasError: false,
    error: "",
  };

  public static getDerivedStateFromError(error: Error): State {
    return {
      hasError: true,
      error: error.message,
    };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error("Error details:", error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return (
        <div
          className={`${classes.error} text-secondary d-flex flex-column align-items-center justify-content-center`}
        >
          <h1>Something went wrong.</h1>
          <p className={classes.errorMsg}>{this.state.error}</p>
          <p className="text-secondary">
            Try refreshing the page, or try again later
          </p>
        </div>
      );
    }

    return this.props.children;
  }
}
