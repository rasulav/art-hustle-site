import Routes from "./routes";

import Layout from "./components/Layout";
import MainNavigation from "./components/Navigation/MainNavigation";
import ErrorBoundary from "./components/ErrorBoundaries";
import Footer from "./components/Footer";

function App() {
  return (
    <ErrorBoundary>
      <Layout header={<MainNavigation />} footer={<Footer />}>
        <Routes />
      </Layout>
    </ErrorBoundary>
  );
}

export default App;
