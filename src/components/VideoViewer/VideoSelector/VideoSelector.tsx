import { ListGroup, Image } from "react-bootstrap";

import videoImage from "../../../assets/images/lesson/selectorVideo.svg";
import classes from "./VideoSelector.module.scss";

interface videos {
  id: number;
  name: string;
  duration: string;
  link: string;
}
interface VideoSelectorProps {
  videos: videos[];
  current: number | null;
}

export const VideoSelector: React.FC<VideoSelectorProps> = (props) => {
  return (
    <ListGroup as="ol" numbered className={classes.videoSelector}>
      {props.videos.map((video, i) => (
        <ListGroup.Item key={video.id} as="li" className={classes.videoItem}>
          <div className={classes.vidImage}>
            <Image src={videoImage} />
          </div>
          <div className={classes.vidDescription + " ms-2 me-auto"}>
            <span className={classes.name}>{video.name}</span>
            <span className={classes.time}>{video.duration}</span>
          </div>
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
};
