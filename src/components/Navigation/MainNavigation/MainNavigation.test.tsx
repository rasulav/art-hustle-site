import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom/extend-expect";

import MainNavigation from "../MainNavigation";

describe("Main Navigation component", () => {
  test('renders "Logout" if the button was clicked', () => {
    render(<MainNavigation />);

    const buttonElement = screen.getByRole("button", { name: /login/i });
    userEvent.click(buttonElement);

    const outputElement = screen.getByText("Logout");
    expect(outputElement).toBeInTheDocument();
  });
});
